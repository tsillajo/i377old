package myPackage;

import java.io.IOException;

//import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class HomePage
 */
public class HomePage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");

		HttpSession session = request.getSession();

		if (session.getAttribute("param") == null) {
			session.setAttribute("param", request.getParameter("param"));
		}

				response.getWriter().println(
				"<!DOCTYPE html>\n" + "<html>\n"
						+ "<head><title>A Test Servlet</title></head>\n"
						+ "<div>Hello </div>"
						+ "<div> Your session id is " + session.getId() + "</div>"
						+ "</div> Session attribute is " + session.getAttribute("param")+ "</div>"
						// + "<div>Session count is " +
						// SessionCount.getTotalActiveSession() +
						// "</div>" +
						+ "</body></html>");
	}
}