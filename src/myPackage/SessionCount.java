package myPackage;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Application Lifecycle Listener implementation class SessionCount
 * 
 */
public class SessionCount extends HttpServlet implements HttpSessionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static int totalActiveSessions = 0;

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		totalActiveSessions++;
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		totalActiveSessions--;
	}

	public static int getTotalActiveSession() {
		return totalActiveSessions;
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().println("count: " + totalActiveSessions);
	}

}
